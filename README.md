# cl-cron-git #

This is a fork of [cl-cron](http://quickdocs.org/cl-cron/api). I will change the name of this project soon. I'll just find a name for it.


### Changes from cl-cron  ###

* Use log4cl as loggin
* Error checking and handeling
* Changing code, to better understand what's going on. 
* Use other libraries like alexandria

### How do I get set up? ###


``` bash

$ cd ~/.roswell/local-projects/
$ git clone https://bitbucket.org/team-knobo/cl-cron.git
$ ros emacs
```


``` cl

(quickload "cl-cron")

(defun init-cron ()
  (log:conf (category :cl-cron) :daily "~/cl-cron.log")
  (setf  cl-cron:*CRON-LOAD-FILE* "~/cl-cron.lisp")
  (cl-cron:start-cron))

(push 'init-cron sb-ext:*init-hooks*)
```