;;; -*- Mode: LISP; Syntax: COMMON-LISP; Package: CL-CRON;

;;;    Copyright (c) 2009, Mackram Ghassan Raydan 
;;;    This file is part of cl-cron.

;;;    cl-cron is free software: you can redistribute it and/or modify
;;;    it under the terms of the GNU General Public License as published by
;;;    the Free Software Foundation, either version 3 of the License, or
;;;    (at your option) any later version.
;;;
;;;    cl-cron is distributed in the hope that it will be useful,
;;;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;    GNU General Public License for more details.
;;;
;;;    You should have received a copy of the GNU General Public License
;;;    along with cl-cron.  If not, see <http://www.gnu.org/licenses/>.


(in-package :cl-cron)


(defparameter *day-list* 
  '(:monday :tuesday :wednesday :thursday :friday :saturday :sunday))

(defparameter *month-list* 
  '(:january :february :march :april :may :june :july :august :september :october :november :december))

(defvar *cron-job-id* 0)

(defclass cron-job ()
  ((minute :accessor job-minute :initarg :job-minute :initform :every)
   (hour :accessor job-hour :initarg :job-hour :initform :every)
   (day-of-month :accessor job-dom :initarg :job-dom :initform :every)
   (month :accessor job-month :initarg :job-month :initform :every)
   (day-of-week :accessor job-dow :initarg :job-dow :initform :every)
   (at-boot :accessor job-at-boot :initarg :job-at-boot :initform nil)
   (function-symbol :accessor job-func :initarg :job-func)
   (name :accessor job-name :initarg :name :initform "cron-job")
   (run  :accessor job-run :initform 0)
   (job-id :accessor job-id :initform (incf *cron-job-id*))))


(defvar *cron-jobs-hash* (make-hash-table) 
  "contains a hash of all corn-job objects that need to be run")

(defvar *cron-dispatcher-thread* nil
  "a parameter to that holds the cron-dispatcher thread")

(defvar *cron-dispatcher-processing* (bt:make-lock)
  "allows us to not kill the thread unless the lock can be acquired")

(defvar *cron-load-file* nil
  "a parameter which points to a lisp or fasl file which would be loaded once start-cron is called. The boot file should be made of as many make-cron-job calls as you like one after the other in normal s-expression fashion.")

(defun make-cron-job (function-symbol &key (minute :every) (step-min 1) (hour :every) (step-hour 1) (day-of-month :every) 
		      (step-dom 1) (month :every) (step-month 1) (day-of-week :every) (step-dow 1) (boot-only nil) (hash-key nil) (name "cron-job"))
  "creates a new instance of a cron-job object and appends it to the cron-jobs-list after processing its time. Note that if you wish to use multiple values for each parameter you need to provide a list of numbers or use the gen-list function. You can not have a list of symbols when it comes to month or day-of-week. Please note that as by ANSI Common Lisp for the month variable the possible values are between 1 and 12 inclusive with January=1 and for day of week the possible values are between 0 and 6 with Monday=0. Returns the hash-key"
  (if (eql hash-key nil) (setf hash-key (gensym "cron")))
  (setf (gethash hash-key *cron-jobs-hash*)
	(make-instance 'cron-job 
		       :job-minute (get-minutes minute step-min)
		       :job-hour (get-hours hour step-hour)
		       :job-dom (get-days-of-month day-of-month step-dom)
		       :job-month (get-months month step-month)
		       :job-dow (get-days-of-week day-of-week step-dow)
		       :job-at-boot boot-only
		       :job-func function-symbol
                       :name name))
  hash-key)


(defun delete-cron-job (cron-key)
  "deletes the cron job with the corresponding hash key"
  (remhash cron-key *cron-jobs-hash*))

(defun time-to-run-job (job &optional (time (get-universal-time)))
  "checks if it is time to run the current job based on the current time"
  (multiple-value-bind (usec umin uhour udom umonth uyear udow)
      (decode-universal-time time)
    (declare (ignore usec uyear))
    (and (member umin (job-minute job))
	 (member uhour (job-hour job))
	 (cond
	   ((and (= (length (job-dom job)) 31)
		  (= (length (job-dow job)) 7))
	    t)
	   ((and (= (length (job-dom job)) 31)
		 (not (= (length (job-dow job)) 7)))
	    (member udow (job-dow job)))	   
	   ((and (not (= (length (job-dom job)) 31))
		 (= (length (job-dow job)) 7))
	    (member udom (job-dom job)))
	   (t
	    (or (member udom (job-dom job))
		(member udow (job-dow job)))))
	 (member umonth (job-month job))
	 (not (job-at-boot job)))))

(defun run (job)
  (with-accessors ((name job-name) (func job-func) (run job-run) (id job-id)) job
    (handler-case
        (cond ((or (functionp func) (fboundp func))
               (log:info 'cron-job "Running: id: ~a, name: ~a, run: ~a" id name (1+ run))
               (funcall func))
              (:otherwise
               (log:error 'cron-job "Not a function ~a" func)))
      (error (e)
        (log:error "Error running cron-job:~a ~a" func e)))))

(defun run-job-if-time (key job)
  "runs the cron-job object in a separate thread if it is its time"
  (declare (ignore key))
  (when (time-to-run-job job)
    (with-accessors ((name job-name) (func job-func) (run job-run) (id job-id)) job
      (bt:make-thread (lambda () (run job)) 
                      :name (format nil "Id: ~a, ~s run:~a" id name (incf run))))))

(defun run-job-if-boot (key job)
  (declare (ignore key))
  "runs the cron-job object in a separate thread if it is a boot job"
  (if (job-at-boot job)
      (bt:make-thread (job-func job) :name (job-name job))))

(defun cron-dispatcher ()
  "function that dispatches the jobs that are ready to be run"
  (do ()
      (nil nil)
    (sleep (time-until-full-minute (get-universal-time)))
    (bt:with-lock-held (*cron-dispatcher-processing*)
      (handler-case
          (maphash #'run-job-if-time *cron-jobs-hash*)
        (error (e) (log:error "Cron dispatcher died: ~a" e))))))

(defun load-cron-file ()
  (log:info 'cron.info "loading cron file: ~a" *cron-load-file*)
  (load *cron-load-file* :verbose nil :print nil :if-does-not-exist nil))

(defun start-cron ()
  "function that starts cron by first loading the cron file defined in the variable, then it runs any cron-job that has the job-only-at-boot property set to t. Finally, it starts a thread that runs cron-dispatcher"
  (cond ((and *cron-dispatcher-thread* (bt:thread-alive-p *cron-dispatcher-thread*))
	 (log:info "You attempted to call start-cron while cron is already loaded and running..."))
	(t
	 (when *cron-load-file*
           (load-cron-file))
         (log:info "Starting cl-cron")
	 (maphash #'run-job-if-boot *cron-jobs-hash*)
	 (setf *cron-dispatcher-thread* (bt:make-thread #'cron-dispatcher :name "cron-dispatcher")))))

(defun restart-cron()
  "function that starts up cron but without loading the file or running any of the boot only cron jobs in the list"
  (if (or (not *cron-dispatcher-thread*) (not (bt:thread-alive-p *cron-dispatcher-thread*)))
      (setf *cron-dispatcher-thread* (bt:make-thread #'cron-dispatcher))
      (log:info "You attempted to call restart-cron while cron is already loaded and running...")))

(defun stop-cron ()
  "allows the stoppage of cron through the killing of the cron-dispatcher. Note that cron-dispatcher is killed only if it is sleeping otherwise we wait till the cron jobs finish. To reuse cron after calling stop-cron, you would need to recall start-cron which would go through all the steps as if cron has just booted. If you wish to prevent these actions when you restart cron then please you restart-cron."
  (bt:with-lock-held (*cron-dispatcher-processing*)
    (cond ((and *cron-dispatcher-thread* (bt:thread-alive-p *cron-dispatcher-thread*))
           (bt:destroy-thread *cron-dispatcher-thread*)
           (setf *cron-dispatcher-thread* nil)
           (log:info "Crond stopped"))
          (t
           (log:info "You attempted to call stop-cron while cron is already stopped...")))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;Utilities for cron that are needed;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmacro def-cron-get-methods (tag-name unit-total-list &optional (allows-symbols nil) (symbol-list nil) (symbol-offset 0))
  `(defun ,tag-name (unit step-unit)
     (cond ((eq unit :every)
	    (elements-within-step ,unit-total-list step-unit))
	   ((and (symbolp unit)
		 ,allows-symbols
		 (not (eq unit :every)))
	    (let ((num (position unit ,symbol-list)))
	      (if (numberp num)
		  (list (+ num ,symbol-offset))
		  (log:error "~a could not find the symbol ~s in its corresponding list." ',tag-name unit))))
	   ((numberp unit)
	    (list unit))
	   ((and (consp unit)
		 (some #'listp unit))   ;; TODO Consider calling ',tag-name recursivvly and concatenate the result.
	    (let ((expanded-list (alexandria:flatten unit)))
	      (if (every #'numberp expanded-list)
		  (elements-within-step expanded-list step-unit)
		  (log:info "~A could not expand the list of data you provided since it contained symbols." ',tag-name))))
	   ((and (consp unit) (mapcan #'numberp unit))
	    (elements-within-step unit step-unit))
           ((functionp unit) (remove-if-not unit ,unit-total-list))
	   (t
	    (log:info "~A could not retrieve an appropriate value from what you offered." ',tag-name)))))

(def-cron-get-methods get-minutes (gen-list 0 59))
(def-cron-get-methods get-hours (gen-list 0 23))
(def-cron-get-methods get-days-of-month (gen-list 1 31))
(def-cron-get-methods get-months (gen-list 1 12) t *month-list* 1)
(def-cron-get-methods get-days-of-week (gen-list 0 6) t *day-list*)

(defun gen-list (start end &optional (increment 1))
  "functions that returns a list of numbers starting with start-list and ending with end-list"
  (if (< start end)
      (loop for i from start to end by increment collect i)
      (loop for i from start downto end by increment collect i)))

(defun min-list (lst)
  "finds the minimum element of a list"
  (when lst
    (reduce 'min lst)))

(defun max-list (lst)
  "finds the minimum element of a list"
  (when lst
    (reduce 'max lst)))
	 
(defun elements-within-step (lst step)
  "function that returns a list of elements that are within a step from each other starting with the first element in the list"
  (intersection (gen-list (min-list lst) (max-list lst) step) lst))

(defun time-until-full-minute (time)
  (let ((seconds (decode-universal-time time)))
    (- 60 seconds)))
