#|
  This file is a part of cl-cron project.
|#

(in-package :cl-user)
(defpackage cl-cron-test-asd
  (:use :cl :asdf))
(in-package :cl-cron-test-asd)

(defsystem cl-cron-test
  :author "Knut Olav Bøhmer"
  :license "GPL"
  :depends-on (:cl-cron
               :prove)
  :components ((:module "t"
                :components
                ((:test-file "cl-cron"))))
  :description "Test system for cl-cron"

  :defsystem-depends-on (:prove-asdf)
  :perform (test-op :after (op c)
                    (funcall (intern #.(string :run-test-system) :prove-asdf) c)
                    (asdf:clear-system c)))

