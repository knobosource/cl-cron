(in-package :cl-user)

(defpackage cl-cron-test
  (:use :cl :cl-cron :prove))

(in-package :cl-cron-test)


(plan 1)

(ok (let ((key (make-cron-job 'list)))
      (unwind-protect
           (let* ((job (gethash key cl-cron::*cron-jobs-hash*))
                  (time (local-time:timestamp-to-universal (local-time:encode-timestamp 0 0 0 0 1 8 2012))))
             (cl-cron::time-to-run-job job time))
        (cl-cron:delete-cron-job key))))

(finalize)

